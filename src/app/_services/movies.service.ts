import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthService} from './auth.service';

const MOVIE_API = 'http://peliculas-backend.test/api/movie/';


@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) { }

  getPopular(): Observable<any> {
    const options = {
      headers: this.getHeaders(),
      params: {
        page: '1'
      },
    };
    return this.http.get(MOVIE_API + 'popular', options);
  }

  private getHeaders(): HttpHeaders {
    return new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.authService.getToken()}`
    });
  }
}
