import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CookieService} from 'ngx-cookie-service';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree} from '@angular/router';

const AUTH_API = 'http://peliculas-backend.test/api/auth/';
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})

export class AuthService implements CanActivate {

  constructor(
    private http: HttpClient,
    private cookies: CookieService,
    public router: Router,
  ) {
  }

  login(username: string, password: string): Observable<any>{
    return this.http.post(AUTH_API + 'login', {
      email: username,
      password
    }, httpOptions);
  }

  register(name: string, username: string, password: string): Observable<any>{
    return this.http.post(AUTH_API + 'signup', {
      name,
      email: username,
      password
    }, httpOptions);
  }

  setToken(token: string): void {
    this.cookies.set('token', token);
  }

  getToken(): string {
    return this.cookies.get('token');
  }

  getUser(): Observable<any> {
    return this.http.get('https://reqres.in/api/users/2');
  }

  canActivate(): boolean {
    if (!this.getToken()) {
        this.router.navigate(['login']);
        return false;
      }
    return true;
  }

}
