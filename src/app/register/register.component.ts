import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../_services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  form = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(6)]),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)])
  });

  constructor(
    public authService: AuthService,
    public router: Router,
  ) { }

  ngOnInit(): void {
  }

  register(): void{
    const name = this.form.controls.name.value;
    const email = this.form.controls.email.value;
    const password = this.form.controls.password.value;
    this.authService.register(name, email, password).subscribe(
      (data) => {
        this.authService.setToken(data.access_token);
        this.router.navigateByUrl('/');
      },
      (error) => {
        if (error.hasOwnProperty('error') ){
          for (const key in error.error.errors) {
            this.form.controls[key].setErrors({taken: !!error.error.errors[key][0]});
          }
        }


      },
    );
  }

}
