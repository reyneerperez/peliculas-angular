import { Component, OnInit } from '@angular/core';
import {MoviesService} from '../_services/movies.service';
import {AuthService} from '../_services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  populares: any;
  constructor(
    private moviesService: MoviesService,
    private authService: AuthService,
    public router: Router,
  ) {
    this.moviesService.getPopular().subscribe(
      data => {
        this.populares = data.results;
        console.log(this.populares);
      }
    );
  }

  ngOnInit(): void {
  }
  logout(): void {
    console.log('salir');
    this.authService.setToken('');
    this.router.navigate(['login']);
  }

}
