import {Component, Input, OnChanges, TemplateRef} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.css']
})
export class MoviesListComponent implements OnChanges {
  @Input() movies: any;
  selected: any = null;
  constructor(private modalService: NgbModal) {
  }

  ngOnChanges(): void {
  }

  showModal(movie: any, content: TemplateRef<any>): void{
    this.selected = movie;
    this.selected.local_rate = this.getRate(movie.id);
    this.modalService.open(content, { size : 'xl' });
  }

  setRate(id: any, rate: number): void {
    const rates = JSON.parse( ( localStorage.getItem('rates')) as string ) ?? [];

    const index = rates.findIndex( ( rate: any ) => rate.id === id);
    if ( index > -1 ){
      rates[index].rate = rate;
    }else{
      rates.push({ id, rate});
    }
    this.selected.local_rate = this.getRate(id);
    localStorage.setItem('rates', JSON.stringify(rates));
  }

  getRate(id: any): number {
    const rates = JSON.parse( ( localStorage.getItem('rates')) as string ) ?? [];
    const rate = rates.find( ( rate: any ) => rate.id === id);
    return rate ? rate.rate : 0;
  }
}
