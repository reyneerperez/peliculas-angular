import { Component, OnInit } from '@angular/core';
import { AuthService  } from '../_services/auth.service';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)])
  });

  constructor(
    public authService: AuthService,
    public router: Router,
  ) {
    if (!!authService.getToken()) {
      router.navigate(['']);
    }
  }

  ngOnInit(): void {
  }

  login(): void {
    const email = this.form.controls.email.value;
    const password = this.form.controls.password.value;
    this.authService.login(email, password).subscribe(
      data => {
        this.authService.setToken(data.access_token);
        this.router.navigateByUrl('/');
      },
      error => {
        console.log(error);
      }
    );
  }

}
